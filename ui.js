class UI {
  constructor() {
    // Header
    this.headerForm = document.getElementById('header-form');
    this.budgetId = document.getElementById('budget-id');
    this.budgetDate = document.getElementById('date-input');
    this.operationCbo = document.getElementById('operation-select');
    this.operatorCbo = document.getElementById('operator-select');
    this.customerCbo = document.getElementById('customer-select');
    // this.searchBtn = document.getElementById('search-btn');
    this.machineCbo = document.getElementById('machine-select');

    // Spare parts
    this.machineForm = document.getElementById('machine-form');
    this.machineCardTitle = document.querySelector('.machine');
    this.partCbo = document.getElementById('part-select');
    this.qtyInput = document.getElementById('qty-input');
    this.aceptBtn = document.getElementById('acept-btn');

    // empty option for select on error
    this.emptyOption = document.createElement('option');
    this.emptyOption.setAttribute('value', '');
    const txt = document.createTextNode('');
    this.emptyOption.appendChild(txt);

    // Result table
    //

    this.apiService = new ApiService();
  }

  setBudgetDate() {
    const d = new Date();
    this.budgetDate.value = d.toLocaleDateString();
  }

  setBudgetId() {
    // get data from API
    this.budgetId.innerText = '000015';
  }

  searchMachinesByCust() {
    console.log('boton search machines by cliente');
  }

  addBudgetDetail() {
    console.log('boton add partes a la lista de detalle');
  }

  sendBudget() {
    console.log('enviar presupuesto');
  }

  async fillCustomersCbo() {
    try {
      const respOne = await this.apiService.getCustomers();
      if (respOne) {
        this.customerCbo.appendChild(this.emptyOption);
        respOne.forEach(cust => {
          const opt = document.createElement('option');
          opt.setAttribute('value', cust.id);
          const txt = document.createTextNode(cust.name);
          opt.appendChild(txt);
          this.customerCbo.appendChild(opt);
          // console.log(cust);
        });
      }
    } catch (error) {
      console.log(error);
    }
  }

  async fillMachinesByCust(custoId) {
    try {
      const respOne = await this.apiService.getMachines(custoId);
      if (respOne) {
        this._removeOptions(this.machineCbo);
        respOne.forEach(machine => {
          const {id, name, ...resto} = machine;
          const opt = document.createElement('option');
          opt.setAttribute('value', id);
          const txt = document.createTextNode(name);
          opt.appendChild(txt);
          this.machineCbo.appendChild(opt);
          // console.log(cust);
        });
      }
    } catch (error) {
      console.log(error);
    }
  }
  // clean all options for select object
  _removeOptions(obj) {
    if (obj == null) return;
    if (obj.options == null) return;
    while (obj.options.length > 0) {
      obj.remove(0);
    }
  }
}
