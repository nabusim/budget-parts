class ApiService {
  constructor() {
    this.baseUrl = 'http://localhost:3001';
    this.customersUrl = `${this.baseUrl}/customers`;
    this.machinesUrl = `${this.baseUrl}/machines`;
  }

  async getCustomer(customerId) {
    const response = await fetch(`${this.customersUrl}/${customerId}}`);
    return await response.json();
  }

  async getCustomers() {
    const response = await fetch(`${this.customersUrl}`);
    return await response.json();
  }

  async getMachines(custId) {
    const response = await fetch(`${this.machinesUrl}?customerId=${custId}`);
    return await response.json();
  }
}
