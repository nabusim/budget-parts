document.addEventListener('DOMContentLoaded', function() {
  // new ui class instance
  const ui = new UI();

  // set initial info
  ui.setBudgetDate();
  ui.setBudgetId();
  ui.fillCustomersCbo();

  // header form submit (btn search)
  ui.headerForm.addEventListener('submit', event => {
    event.preventDefault();
    ui.searchMachinesByCust();
  });

  // Acept part button
  ui.aceptBtn.addEventListener('click', event => {
    event.preventDefault();
    if (event.target.parentElement.id === 'acept-btn') {
      ui.addBudgetDetail();
    }
  });

  // budget form submit
  ui.machineForm.addEventListener('submit', event => {
    event.preventDefault();
    ui.sendBudget();
  });

  // Cbo customer listener
  ui.customerCbo.addEventListener('change', event => {
    // console.log(event.target.value);
    ui.fillMachinesByCust(event.target.value);
  });
});
